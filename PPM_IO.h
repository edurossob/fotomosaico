#ifndef __PP_IO__
#define __PP_IO__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tile.h"
#include "manipulaPPM.h"

int carregaImagemEntrada(FILE** arq, char* imagePath, int *sizeH, int *sizeL, int *type, int* maxPixelValue);

/*
    Transforma imagem ppm em vetor de tiles de tamanho tamanhoTile.
    A função ira descobrir os metadados da imagem (altura, largura, tipo e valor máximo de RGB)
*/
int transformaEmTile(char* imagePath, tile** vetorTiles, int tamanhoTile, int *maxImgTile, int* imageHigh, int *imageWidth, int *imageType);

// Procura em vetorTiles o tile mais proximo de cada item do vetorImagem e cadastra seu caminho em vetorImagem[i].path
int calculaMaisProximo(tile** vetorImagem, tile* vetorTiles, int qntImagem, int qntTiles);

/* Imprime em saida vetor de tiles de tamanho tamanhoTile em formato de uma imagem ppm do tipo imageType,
    imagem esta de dimensoes imageHigh x imageWidth 
*/
int imprimeSaida(char* saida, tile* vetorTile, int tamanhoTile,  int imageHigh, int imageWidth, int imageType);

// Desaloca arquivo arq
int fechaImagemEntrada(FILE* arq);

#endif