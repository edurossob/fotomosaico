#include "manipulaPPM.h"


// Retorna 1 caso seja um comentário e 0 caso contrário
int isComentario(char* linha, size_t len){
    for (int i = 0; linha[i] != '\0'; i++)
        if(linha[i] == '#')
            return 1;
    return 0;
}

// Caso tipo seja P3 retorna 0, caso seja P6 retorna 1, caso contrário -1
int selecionaTipo(char* tipo){
    if(!strncmp(tipo, "P3", 2))
        return 0;
    if(!strncmp(tipo, "P6", 2))
        return 1;
    return -1;
}

int leMeta(FILE* arq, int* sizeH, int* sizeL, int *type, int* maxPixelValue){
    char *linha = NULL;
    size_t len = 0;

    int i = 0; // contador

    while (i < 3) {
        getline(&linha, &len, arq);
        if(!isComentario(linha, len)){
            switch (i)
            {
            case 0:
                *type = selecionaTipo(linha);
                break;
            case 1:
                sscanf(linha, "%d %d", sizeL, sizeH);
                break;
            case 2:
                sscanf(linha, "%d", maxPixelValue);
                break;
            default:
                break;
            }
            i++;
        }
        
    }

    free(linha);
    return 1;
}

int calculaRGBP6_linha(FILE* arq, int tamanhoN, int *r, int *g, int *b){

    unsigned char tempR, tempG, tempB; //valores rgb temporarios utilizados para calculo da média
    tempR = 0;
    tempG = 0;
    tempB = 0;

    for (int i = 0; i < tamanhoN; i++){
        if(feof(arq))
            break;
        if(ferror(arq))
            break;

        fread (&tempR, 1, 1, arq); 
        fread (&tempG, 1, 1, arq);
        fread (&tempB, 1, 1, arq);


        if(*r == 0)
            *r = (int)tempR;
        else
            *r = (*r + (int)tempR)/2;
        
        if(*g == 0)
            *g = (int)tempG;
        else
            *g = (*g + (int)tempG)/2;

        if(*b == 0)
            *b = (int)tempB;
        else
            *b = (*b + (int)tempB)/2;
    }

    return 1;
}

int calculaRGBP6(FILE* arq, int tamanhoN, int *r, int *g, int *b){

    for (int i = 0; i < tamanhoN; i++){
        if(feof(arq))
            break;
        if(ferror(arq))
            break;

        calculaRGBP6_linha(arq, tamanhoN, r, g, b);
    }

    return 1;
}

int calculaRGBP3_linha(FILE* arq, int tamanhoN, int *r, int *g, int *b){

    *r = 0;
    *g = 0;
    *b = 0;


    int tempR, tempG, tempB; //valores rgb temporarios utilizados para calculo da média
    tempR = 0;
    tempG = 0;
    tempB = 0;


    for (int i = 0; i < tamanhoN; i++) {
        if(ferror(arq)){
            perror("ferror");
            break;
        }
        if(feof(arq)){
            perror("feof rgbp3_linha");
            return -1;
            break;
        }
        
        fscanf(arq, "%d", &tempR);
        fscanf(arq, "%d", &tempG);
        fscanf(arq, "%d", &tempB);


        if(*r == 0)
            *r = tempR;
        else
            *r = (*r + tempR)/2;
        
        if(*g == 0)
            *g = tempG;
        else
            *g = (*g + tempG)/2;

        if(*b == 0)
            *b = tempB;
        else
            *b = (*b + tempB)/2;
    }
return 1;
}

int calculaRGBP3(FILE* arq, int tamanhoN, int *r, int *g, int *b){

    for (int i = 0; i < tamanhoN; i++) {
        if(ferror(arq)){
            perror("ferror");
            break;
        }
        if(feof(arq)){
            perror("feof rgbp3");
            break;
        }
        
        calculaRGBP3_linha(arq, tamanhoN, r, g, b);
    }
    return 1;
}

int isPPM(char* nomeArquivo){

    int lenght = strlen(nomeArquivo);

    const char* extensaoPPM = ".ppm";

    for (int i = lenght - 4; i < lenght; i++)
        if(extensaoPPM[i - lenght + 4] != nomeArquivo[i])
            return 0;
    return 1;
}

int escreveMeta(FILE* arq, int tipo, int tamanhoW, int tamanhoH, int maxValue){
    if(tipo) // caso tipo == 1 == P6
        fprintf(arq, "P6\n");
    else // caso contrario (tipo == 0 == P3)
        fprintf(arq, "P3\n");

    fprintf(arq, "%d %d\n", tamanhoH, tamanhoW);
    fprintf(arq, "%d\n", maxValue);
        
    return 1;
}


int imprimeLinhaP3(FILE* entrada, FILE* saida, int tileType, int linha, int nColunas, int tamanhoTile){
    int tempR, tempG, tempB; //valores rgb temporarios utilizados para impressão na saida
    unsigned char tempRchar, tempGchar, tempBchar; //valores rgb temporarios utilizados para impressão na saida
    int count = 0;


    // Lê e ignora esses itens
    for(int i = 0; i < linha; i++){
        for(int j = 0; j < tamanhoTile; j++){
            if(feof(entrada))
                perror("End of file");

            if(tileType) { // Caso seja == 1 == P6
                fread (&tempRchar, 1, 1, entrada); 
                fread (&tempGchar, 1, 1, entrada);
                fread (&tempBchar, 1, 1, entrada);

            } else {
                fscanf(entrada, "%d", &tempR);
                fscanf(entrada, "%d", &tempG);
                fscanf(entrada, "%d", &tempB);
            }
            
            count++;
        }
    }


    for(int i = 0; i < nColunas; i++){
        if(tileType) { // Caso seja == 1 == P6
            fread (&tempRchar, 1, 1, entrada); 
            fread (&tempGchar, 1, 1, entrada);
            fread (&tempBchar, 1, 1, entrada);

            fprintf(saida, "%d ", tempRchar);
            fprintf(saida, "%d ", tempGchar);
            fprintf(saida, "%d ", tempBchar);
        } else {
            fscanf(entrada, "%d", &tempR);
            fscanf(entrada, "%d", &tempG);
            fscanf(entrada, "%d", &tempB);

            fprintf(saida, "%d ", tempR);
            fprintf(saida, "%d ", tempG);
            fprintf(saida, "%d ", tempB);
        }
    }

    fprintf(saida, "\n");

    return 1;
}

int imprimeLinhaP6(FILE* entrada, FILE* saida, int tileType, int linha, int nColunas, int tamanhoTile){
    int tempR, tempG, tempB; //valores rgb temporarios utilizados para impressão na saida
    unsigned char tempRchar, tempGchar, tempBchar; //valores rgb temporarios utilizados para impressão na saida
    int count = 0;


    // Lê e ignora esses itens
    for(int i = 0; i < linha; i++){
        for(int j = 0; j < tamanhoTile; j++){
            if(feof(entrada))
                perror("End of file");

            if(tileType) { // Caso seja == 1 == P6
                fread (&tempRchar, 1, 1, entrada); 
                fread (&tempGchar, 1, 1, entrada);
                fread (&tempBchar, 1, 1, entrada);

            } else {
                fscanf(entrada, "%d", &tempR);
                fscanf(entrada, "%d", &tempG);
                fscanf(entrada, "%d", &tempB);
            }
            
            count++;
        }
    }


    for(int i = 0; i < nColunas; i++){
        if(tileType) { // Caso seja == 1 == P6
            fread (&tempRchar, 1, 1, entrada); 
            fread (&tempGchar, 1, 1, entrada);
            fread (&tempBchar, 1, 1, entrada);

            fwrite(&tempRchar, 1, 1, saida);
            fwrite(&tempGchar, 1, 1, saida);
            fwrite(&tempBchar, 1, 1, saida);
        } else {
            fscanf(entrada, "%d", &tempR);
            fscanf(entrada, "%d", &tempG);
            fscanf(entrada, "%d", &tempB);

            fwrite(&tempR, 1, 1, saida);
            fwrite(&tempG, 1, 1, saida);
            fwrite(&tempB, 1, 1, saida);
        }
    }

    return 1;
}