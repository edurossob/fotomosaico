#ifndef __TILES__
#define __TILES__
    
    #include <dirent.h>
    #include <stdlib.h>
    #include "manipulaPPM.h"

    #define MAX_PATH_SIZE 1024

    typedef struct {
        int red;
        int green;
        int blue;
        char path[MAX_PATH_SIZE - 1];
    } tile;

    // Imprime um tile
    int imprimeTile(tile pastilha);

    // Imprime um vetor de tiles
    int imprimeTiles(tile* vetorTiles, int qntTiles);

    /*
     Carrega os tiles da pasta encontrada em tilesPath, calculando suas medias RGB. 
     Descobre a quantidade de tiles e os aloca em um vetor de tamanho maxQntTiles
    */
    int carregaTiles(char* tilesPath, tile* vetorTiles[], int *qntTiles, unsigned int *maxQntTiles, int *tamanhoTile);
    
    // Aloca um array de tiles de tamanho n e retorna seu endereço
    tile* alocaArrayTiles(int n);

    // Cadastra tile com valores red, green, blue e tilePath
    void pushTile(char* tilePath, tile* pastilha, int red, int green, int blue);

    // Libera alocação dinamica do vetor vetorTiles
    int destroiTiles(tile* vetorTiles[]);




#endif