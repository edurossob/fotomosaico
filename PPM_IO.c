#include "PPM_IO.h"

int carregaImagemEntrada(FILE** imagemBase, char* imagePath, int *imageHigh, int *imageWidth, int *imageType, int* maxRGBValue){
    FILE* arq;

    if(strncmp(imagePath, "stdin", 7))
        arq = fopen(imagePath, "r");
    else
        arq = stdin;
    
    
    if( ! arq){
        fprintf(stdout, "Impossivel abrir arquivo de imagem base em %s\n", imagePath);
        perror("Abortando");
        exit(1);
    }

    leMeta(arq, imageHigh, imageWidth, imageType, maxRGBValue);

    *imagemBase = arq;

    return 1;
}


int imprimeSaida(char* caminhoSaida, tile* vetorTile, int tamanhoTile,  int imageHigh, int imageWidth, int imageType){
    FILE* arqSaida;
    FILE* pastilha;
    
    arqSaida = stdout;
    int trash;
    int tileType;

    int qntTilesW;
    int qntTilesH;
    double qntTilesWReal;
    double qntTilesHReal;


    if(strncmp(caminhoSaida, "stdout", 6)) // Caso ele não seja stdout
        arqSaida = fopen(caminhoSaida, "w");

    if(!arqSaida){
        perror("Arquivo de saída não encontrado ou corrompido");
        exit(1);
    }

    escreveMeta(arqSaida, imageType, imageHigh, imageWidth, 255);

    qntTilesH = (imageHigh / tamanhoTile);
    qntTilesW = (imageWidth / tamanhoTile);
    qntTilesHReal = ((double)imageHigh / (double)tamanhoTile);
    qntTilesWReal = ((double)imageWidth / (double)tamanhoTile);

    if( qntTilesHReal > qntTilesH) // Verifica se cabe alguma imagem quebrada em baixo
        qntTilesH ++;

    if( qntTilesWReal > qntTilesW) // Verifica se cabe alguma imagem quebrada em baixo
        qntTilesW ++;

    for(int count = 0; count < qntTilesH; count ++){
        for(int i = 0; i < tamanhoTile ; i++){
            for(int j = 0; j < qntTilesW; j++){

                pastilha = fopen(vetorTile[ count * qntTilesW + j].path , "r+");

                if(!pastilha){
                    perror("pastilha não existe");
                    exit(1);
                }

                leMeta(pastilha, &trash, &trash, &tileType ,&trash);
                
                if(qntTilesWReal - j < 1){
                    if(imageType)   // caso seja == 1 == P6
                        imprimeLinhaP6(pastilha, arqSaida, tileType, i, imageWidth - j* tamanhoTile, tamanhoTile);
                    else            // Caso contrario
                        imprimeLinhaP3(pastilha, arqSaida, tileType, i, imageWidth - j* tamanhoTile, tamanhoTile);
                } else {
                    if(imageType)   // caso seja == 1 == P6
                        imprimeLinhaP6(pastilha, arqSaida, tileType, i, tamanhoTile, tamanhoTile);
                    else            // caso contrário
                        imprimeLinhaP3(pastilha, arqSaida, tileType, i, tamanhoTile, tamanhoTile);
                }
                fclose(pastilha);
            }
        }
    }
    fclose(arqSaida);
    return 1;
}


int transformaEmTile(char* imagePath, tile** vetorTiles, int tamanhoTile, int* maxImgTile, int* imageHigh, int *imageWidth, int* imageType){  
    FILE* imagemBase;
    int maxRGBValue;

    int r = 0;
    int g = 0;
    int b = 0;

    // Quantidade de tiles que cabem em width e high (Teto)
    int qntTilesW;
    int qntTilesH;
    double qntTilesWReal;
    double qntTilesHReal;

    int err;

    carregaImagemEntrada(&imagemBase, imagePath, imageHigh, imageWidth, imageType, &maxRGBValue);

    qntTilesH = (*imageHigh / tamanhoTile);
    qntTilesW = (*imageWidth / tamanhoTile);
    qntTilesHReal = ((double)*imageHigh / (double)tamanhoTile);
    qntTilesWReal = ((double)*imageWidth / (double)tamanhoTile);

    if( qntTilesHReal > qntTilesH) // Verifica se cabe alguma imagem quebrada em baixo
        qntTilesH ++;

    if( qntTilesWReal > qntTilesW) // Verifica se cabe alguma imagem quebrada em baixo
        qntTilesW ++;

    *maxImgTile = qntTilesH * qntTilesW;

    *vetorTiles = alocaArrayTiles(*maxImgTile); // Aloca exatamente quantas pastilhas cabem na imagem 

    for(int count = 0; count < qntTilesH; count ++){
        for(int i = 0; i < tamanhoTile ; i++){
            for(int j = 0; j < qntTilesW; j++){

                if(qntTilesWReal - j < 1){
                    if(*imageType == 1)   // caso seja == 1 == P6
                        err = calculaRGBP6_linha(imagemBase, *imageWidth - j* tamanhoTile, &r, &g, &b);
                    else            // Caso contrario
                        err = calculaRGBP3_linha(imagemBase, *imageWidth - j* tamanhoTile,&r, &g, &b);
                } else {
                    if(*imageType == 1)   // caso seja == 1 == P6
                        err = calculaRGBP6_linha(imagemBase, tamanhoTile, &r, &g, &b);
                    else            // caso contrário
                        err = calculaRGBP3_linha(imagemBase, tamanhoTile,&r, &g, &b);
                }

                if(err == -1)
                    break;


                pushTile(imagePath, &vetorTiles[0][ count * qntTilesW + j], r, g, b);

            }
        }
    }

    //fechaImagemEntrada(&imagemBase);
    if( ! imagemBase){
        fprintf(stdout, "Impossivel fechar imagem\n");
        perror("Abortando");
        exit(1);
    }
    fprintf(stderr, "Fechando imagem base.\n");
    fclose(imagemBase);

    return 1;
}

// calcula a distancia de cor entre os tiles a e b
int redMean(tile a, tile b){
    float drs, dgs, dbs, dc; // deltas ao quadrado(sqared) de r g e b, e delta das cores (resultado)
    float mr; // media r, ou, r barra, ou, redmean

    drs = ((double)a.red - (double)b.red) * ((double)a.red - (double)b.red);
    dgs = ((double)a.green - (double)b.green) * ((double)a.green - (double)b.green);
    dbs = ((double)a.blue - (double)b.blue) * ((double)a.blue - (double)b.blue);

    mr = ((double)a.red + (double)b.red) / 2; 

    dc = sqrt( ( ((2+(mr/256)) * drs) + (4 * dgs )+( (2+( (255-mr)/256 ))) + dbs));

    return dc;
}

int calculaMaisProximo(tile** vetorImagem, tile* vetorTiles, int qntImagem, int qntTiles){
    int menorDistancia; // menor distancia encontrada
    int maisProximo; // posição do tile mais proximo

    int distancia; 

    for(int i = 0; i <  qntImagem; i++){    // Caminha de pastilha em pastilha da imagem base
        menorDistancia = 10000; // Inicia em 10.000 arbitrariamente
        maisProximo = 0;
        for (int j = 0; j < qntTiles; j++){ // Compara com todos os tiles

            distancia = redMean(vetorImagem[0][i], vetorTiles[j]); 
            
            if(distancia < menorDistancia){
                menorDistancia = distancia;
                maisProximo = j;
            }
        }
        pushTile(vetorTiles[maisProximo].path, &vetorImagem[0][i], vetorImagem[0][i].red, vetorImagem[0][i].green, vetorImagem[0][i].blue);
    }

    return 1;
}




int fechaImagemEntrada(FILE* arq){
    if( ! arq){
        fprintf(stdout, "Impossivel fechar imagem\n");
        perror("Abortando");
        exit(1);
    }
    fclose(arq);
    return 1;
}