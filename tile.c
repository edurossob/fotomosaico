#include "tile.h"
#include <stdio.h>

void pushTile(char* tilePath, tile* pastilha, int red, int green, int blue){
    pastilha->red = red;
    pastilha->green = green;
    pastilha->blue = blue;
    strcpy(pastilha->path, tilePath);
}

int imprimeTile(tile pastilha){
    fprintf(stderr, " |tile.r = %d\n |tile.g = %d\n |tile.b = %d\n |tile.path = %s\n", 
            pastilha.red,pastilha.green,pastilha.blue, pastilha.path);
    return 1;
}

int imprimeTiles(tile* vetorTiles, int qntTiles){
    for (int i = 0; i < qntTiles; i++){
        imprimeTile(vetorTiles[i]);
    }
    return 1;
}


tile* alocaArrayTiles(int n){

    tile* t = malloc(n*sizeof(tile));

    for (int i = 0; i < n; i++)
        pushTile("pushInicial", &t[i], 0, 1, 2);

    return t;
}

tile** realocaArrayTiles(tile ** t, int n){

    *t = realloc(*t, n*sizeof(tile));

    return t;
}

int carregaTiles(char* tilesPath, tile** vetorTiles, int *qntTiles, unsigned int *maxQntTiles, int *tamanhoTile){
    DIR *tilesFolder;
    struct dirent *entry;
    FILE* arq;

    int maxPixelValue = 0;

    char caminho[MAX_PATH_SIZE - 1]; // caminho do tile
    int altura = 0, largura = 0; // tamanho do tile
    int tipo = -1; // Tipo do arquivo (P3 = 0, P6 = 1)

    int i = 0; // Contador de tiles (arquivos ppm/ no diretório 
    int r = 0, g = 0, b = 0;


    tilesFolder = opendir(tilesPath);

    if(!tilesFolder){
        perror("Impossível abrir diretório");
        return -1;
    }


    while( ( entry = readdir(tilesFolder) ) ) {

        if (i >= *maxQntTiles){ // Caso não caiba no array alocado atual:
            *maxQntTiles *= 2;  // Dobrar o tamanho máximo
            vetorTiles = realocaArrayTiles(vetorTiles, *maxQntTiles);
        }

        if (isPPM(entry->d_name)){ // Caso tenha a extensão .ppm
            strcpy(caminho, tilesPath);
            strcat(caminho, entry->d_name);


            // manuseando arquivo ppm
            arq = fopen(caminho, "r");

            if( ! arq ){
                perror("Impossivel abrir arquivo");
                return -1;
            }

            leMeta(arq, &altura, &largura, &tipo, &maxPixelValue);

            *tamanhoTile = altura;

            if(tipo == 0)
                calculaRGBP3(arq, *tamanhoTile, &r, &g, &b);
            if(tipo == 1)
                calculaRGBP6(arq, *tamanhoTile, &r, &g, &b);
            if(tipo == -1){
                fprintf(stderr, "Existe algum tile que não é P3 nem P6\n");
                return -1;
            }

            pushTile(caminho, &vetorTiles[0][i], r, g, b);
            fclose(arq);            
            
            i++;
        }    
    }

    closedir(tilesFolder);

    *qntTiles= i;

    return 1;        
}

int destroiTiles(tile * vetorTiles[]){
    free(*vetorTiles);

    return 1;
}
