#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "tile.h"
#include "PPM_IO.h"

int main(int argc, char *const *argv){
    int opt;

    char imagePath[MAX_PATH_SIZE];  // | Variaveis da imagem base
    int imageWidth, imageHigh;      // |
    int imageType;     // |
    int maxImgTiles = 0;              // | Quantidade máxima de tiles
    tile *imagemTiles;              // |


    char tilesPath[MAX_PATH_SIZE];   // | Variaveis dos tiles
    tile *vetorTiles;                // |
    int tamanhoTile;
    int qntTiles;                    // | Quantos tiles temos carregados e disponíveis
    unsigned int maxQntTiles = 7;    // | Quantidade máxima de Tiles (Inicia em 7 arbitrariamente)


    char imageSaidaPath[MAX_PATH_SIZE];
    
    int err; // Variável para erros

    strncpy(tilesPath, "./tiles/", MAX_PATH_SIZE);
    strncpy(imagePath, "stdin", MAX_PATH_SIZE);
    strncpy(imageSaidaPath, "stdout", MAX_PATH_SIZE);

    // Verifica opções
    while ((opt = getopt(argc, argv, "i:o:p:h")) != -1 ){
        switch (opt) {
        case 'i':
            strncpy(imagePath, optarg, MAX_PATH_SIZE-1);
            break;
        case 'o':
            strncpy(imageSaidaPath, optarg, MAX_PATH_SIZE-1);
            break;
        case 'p':
            strncpy(tilesPath, optarg, MAX_PATH_SIZE);
            strcat(tilesPath, "/");
            break;
        case 'h':
            fprintf(stderr, "Esse programa transforma fotos em fotomosaicos!\n");
            fprintf(stderr, "Exemplo de chamada: $ mosaico [ -p diretório de pastilhas ] [ -i arquivo ] [ -o arquivo ]\n");
            fprintf(stderr, "Opções:\n");
            fprintf(stderr, "-i : indica a imagem de entrada; se não for informada, assume-se a entrada padrão (stdin).\n");
            fprintf(stderr, "-o : indica a imagem de saída; se não for informada, assume-se a saída padrão (stdout).\n");
            fprintf(stderr, "-p : indica o diretório de pastilhas; se não for informado, assume-se o diretório ./tiles.\n");
            fprintf(stderr, "-h : gera uma mensagem de ajuda na saída de erro (stderr), explicando o que o programa faz e quais as opções disponíveis.\n");
            exit(0);
            break;
        default:
            fprintf(stderr, "Opção desconhecida na chamada!\n");
            break;
        }   
    }

    fprintf(stderr, "Reading tiles from %s\n", tilesPath);

    // Carrega Tiles
    vetorTiles = alocaArrayTiles(maxQntTiles); // Alocação inicial


    fprintf(stderr, "Calculating tiles' average colors\n");

    err = carregaTiles(tilesPath, &vetorTiles, &qntTiles, &maxQntTiles, &tamanhoTile);

    if(err < 0){
        fprintf(stderr, "Abortando programa\n");
        destroiTiles(&vetorTiles);
        exit(1);
    }
    
    fprintf(stderr, "%d tiles read\n", qntTiles);
    fprintf(stderr, "Tile size is %dx%d\n", tamanhoTile, tamanhoTile);


    // Manuseia imagem base
    imageHigh = 0;
    imageWidth = 0;

    fprintf(stderr, "Reading input image\n");

    transformaEmTile(imagePath, &imagemTiles, tamanhoTile, &maxImgTiles, &imageHigh, &imageWidth, &imageType);

    if(imageType) // case seja == 1 == P3
        fprintf(stderr, "Input image is PPM P3, %dx%d pixels\n", imageWidth, imageHigh);
    else    // caso contrario ( == 0 == P6)
        fprintf(stderr, "Input image is PPM P6, %dx%d pixels\n", imageWidth, imageHigh);


    fprintf(stderr, "Building mosaic image\n");
    calculaMaisProximo(&imagemTiles, vetorTiles, maxImgTiles, qntTiles);

    fprintf(stderr, "Writing output file\n");
    imprimeSaida(imageSaidaPath ,imagemTiles, tamanhoTile, imageHigh, imageWidth, imageType);

    destroiTiles(&vetorTiles);
    destroiTiles(&imagemTiles);

    fprintf(stderr, "Success!\n");
    return 0;
}