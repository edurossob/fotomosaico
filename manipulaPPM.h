#ifndef __manipulaPPM__
#define __manipulaPPM__

    #include <string.h>
    #include <stdio.h>  
    #include <stdlib.h>

    // Verfica se uma string termina em ".ppm", se sim retorna 1, 0 caso contrário
    int isPPM(char* nomeArquivo);

    // Lê metadados do arquivo ppm
    int leMeta(FILE* arq, int *sizeH, int *sizeL, int *type, int* maxPixelValue);

    // Calcula a média rgb de uma linha de tamanho tamanhoN de um arquivo ppm p3, já aberto
    int calculaRGBP6_linha(FILE* arq, int tamanhoN, int *r, int *g, int *b);

    // Calcula a média rgb de uma linha de tamanho tamanhoN de um arquivo ppm p6, já aberto
    int calculaRGBP3_linha(FILE* arq, int tamanhoN, int *r, int *g, int *b);

    // Calcula a média rgb de um arquivo ppm p3, já aberto, de dimensoes tamanhoN x tamanhoN
    int calculaRGBP3(FILE* arq, int tamanhoN, int *r, int *g, int *b);

    // Calcula a média rgb de um arquivo ppm p6, já aberto, de dimensoes tamanhoN x tamanhoN
    int calculaRGBP6(FILE* arq, int tamanhoN, int *r, int *g, int *b);
  
    // Escreve os metadados no arquivo de formato adequado
    int escreveMeta(FILE* arq, int tipo, int tamanhoW, int tamanhoH, int maxValue);

    // Escreve nColunas de determinada linha do arquivo de entrada no arquivo P3 de saída. Cada linha possui tamanhoTile de valores rgb
    int imprimeLinhaP3(FILE* entrada, FILE* saida, int tileType, int linha, int nColunas, int tamanhoTile);

    // Escreve nColunas de determinada linha do arquivo de entrada no arquivo P6 de saída. Cada linha possui tamanhoTile de valores rgb
    int imprimeLinhaP6(FILE* entrada, FILE* saida, int tileType, int linha, int nColunas, int tamanhoTile);


#endif