CFLAGS = -Wall     # flags de compilacao
LDLIBS = -lm       # bibliotecas a ligar

objs = mosaico.o tile.o manipulaPPM.o PPM_IO.o

all: mosaico

# Regra de ligacaoM
mosaico: $(objs)

mosaico.o: mosaico.c tile.h manipulaPPM.h

tile.o: tile.c tile.h manipulaPPM.h

manipulaPPM.o: manipulaPPM.c manipulaPPM.h

PPM_IO.h: PPM_IO.c PPM_IO.h

clean:
	-rm -f *~ *.o

purge: clean
	-rm -f mosaico
